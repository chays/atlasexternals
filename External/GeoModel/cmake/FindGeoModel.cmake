# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Locate the GeoModel external package.
#
# Defines:
#  GEOMODEL_FOUND
#  GEOMODEL_INCLUDE_DIR
#  GEOMODEL_INCLUDE_DIRS
#  GEOMODEL_<component>_FOUND
#  GEOMODEL_<component>_LIBRARY
#  GEOMODEL_LIBRARIES
#  GEOMODEL_LIBRARY_DIRS
#
# The user can set GEOMODEL_ROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME GeoModel
   INCLUDE_SUFFIXES include 
   INCLUDE_NAMES GeoModelKernel/RCBase.h
   		 GeoGenericFunctions/AbsFunction.h
   LIBRARY_SUFFIXES lib
   DEFAULT_COMPONENTS GeoModelKernel GeoGenericFunctions )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( GeoModel DEFAULT_MSG GEOMODEL_INCLUDE_DIR
   GEOMODEL_INCLUDE_DIRS GEOMODEL_LIBRARIES )
mark_as_advanced( GEOMODEL_FOUND GEOMODEL_INCLUDE_DIR GEOMODEL_INCLUDE_DIRS
   GEOMODEL_LIBRARIES GEOMODEL_LIBRARY_DIRS )
