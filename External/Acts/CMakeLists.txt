# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building Acts as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Acts )

find_package( Eigen )
find_package( Boost 1.62.0 REQUIRED )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
#if( NOT ATLAS_BUILD_Acts )
#   return()
#endif()

# Tell the user what's happening:
message( STATUS "Building Acts as part of this project" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ActsBuild )

# Build Acts for the build area:
ExternalProject_Add( Acts
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   GIT_REPOSITORY https://gitlab.cern.ch/acts/acts-core.git
   GIT_TAG "v0.07.03"
   
   CMAKE_CACHE_ARGS 
   -DACTS_BUILD_MATERIAL_PLUGIN:STRING=ON
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DBOOST_ROOT:PATH=${BOOST_ROOT}
   -DEIGEN_INCLUDE_DIR_HINTS:PATH=${EIGEN_INCLUDE_DIR}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Acts purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Acts"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Acts forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of Acts"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Acts buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Acts into the build area"
   DEPENDEES install )
add_dependencies( Package_Acts Acts )

# And now install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
