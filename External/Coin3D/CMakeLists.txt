# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building SoQt for ATLAS.
#

# The package's name:
atlas_subdir( Coin3D )

# Set up its package dependencies: 
atlas_depends_on_subdirs(    
    PRIVATE Externals/Simage )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/coin3dBuild )

# The source code for Coin3D:
set( _source "http://cern.ch/atlas-software-dist-eos/externals/Coin3D/coin-source.v2.zip" )
set( _md5 "1cfd2550a611c78500440038a72e6352" )

# Set up the build of Coin3D:
ExternalProject_Add( Coin3D
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}

   URL ${_source}
   URL_MD5 ${_md5}

   CMAKE_CACHE_ARGS 
   -DSIMAGE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DCMAKE_PREFIX_PATH:PATH=${Simage_ROOT}
   -DSIMAGE_RUNTIME_LINKING:BOOL=ON
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   
   LOG_CONFIGURE 1
)
ExternalProject_Add_Step( Coin3D forceconfigure
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the configuration of Coin3D"
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Coin3D buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing Coin3D into the build area"
   DEPENDEES install 
)

add_dependencies( Coin3D Simage )
add_dependencies( Package_Coin3D Coin3D )

# Install Coin3D:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
