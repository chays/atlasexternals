# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthDerivationExternals.
#
+ External/CheckerGccPlugins
+ External/CLHEP
+ External/FFTW
+ External/FastJet
+ External/FastJetContrib
+ External/Gdb
+ External/GPerfTools
+ External/GoogleTest
+ External/MKL
+ External/dSFMT
+ External/lwtnn
+ External/yampl
+ External/Lhapdf
- .*
